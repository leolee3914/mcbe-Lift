# Lift 升降機插件

這是我第一個用notepad++寫的插件，寫得好難看，我不會再重新整理，可能也不會再有更新

## 適用伺服器版本
mcbe 1.5 (伺服器核心經常有修改，自己測試啦)

## 使用方法

### 增加升降機
寫木牌
```
lift
(移動格數)
(目前層數)
(留空)
```

### 移除升降機
在升降機木牌的上一格或者下一格寫木牌，然後點擊
```
del
del
del
del
```

## Copyright (c) 2018 Lee Siu San

[https://gitlab.com/leolee3914](https://gitlab.com/leolee3914)