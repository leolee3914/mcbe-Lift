<?php

namespace Lift;

use pocketmine\level\sound\LaunchSound;
use pocketmine\level\sound\ClickSound;
use pocketmine\level\sound\FizzSound;
use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\level\Position;
use pocketmine\level\Level;
use pocketmine\entity\Arrow ;
use pocketmine\level\Location;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\inventory\PlayerInventory;
use pocketmine\Server;
use pocketmine\level\particle\FloatingTextParticle;
use pocketmine\OfflinePlayer;
use pocketmine\utils\Config;
use pocketmine\command\ConsoleCommandSender;
use pocketmine\math\Vector3;
use pocketmine\scheduler\PluginTask;
use pocketmine\scheduler\CallbackTask;
use pocketmine\block\Block;
use pocketmine\event\entity\EntityDeathEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\tile\Sign;
use pocketmine\tile\Tile;
use pocketmine\utils\TextFormat;
use pocketmine\event\player\PlayerDeathEvent;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\event\block\SignChangeEvent;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\block\BlockPlaceEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;
use pocketmine\event\player\PlayerRespawnEvent;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\inventory\Inventory;



class Lift extends PluginBase implements Listener{
	private static $instance;

	public static function getInstance(){
		return static::$instance;
	}

	public function onEnable(){
		if(!static::$instance instanceof Lift){
			static::$instance = $this;
		}
		$this->getServer()->getPluginManager()->registerEvents($this, $this);
		
    }
	
	public function onSignChange(SignChangeEvent $event){
		
		if ( ($event->getBlock()->getID() == 63 ) || ($event->getBlock()->getID() == 68) || ($event->getBlock()->getID() == 323)){
		
		if($event->getLine(0) == "Lift" || $event->getLine(0) == "升降機" || $event->getLine(0) == "lift" || $event->getLine(0) == "LIFT" || $event->getLine(0) == TextFormat::YELLOW ."升降機"){
		$p = $event->getPlayer();
		$event->setLine(0, TextFormat::YELLOW ."[升降機]");
		
		$event->setLine(3, TextFormat::YELLOW ."製作：". $event->getPlayer()->getName());
		
		if(is_numeric($event->getLine(1)) === false){
			$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的移動格數");
			$event->setLine(1, TextFormat::RED ."移動格數錯誤！");
		}
		
		if(is_numeric($event->getLine(2)) === false){
			$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的層數");
			$event->setLine(2, TextFormat::RED ."層數錯誤！");
		}else{
			$event->setLine(2, TextFormat::YELLOW ."目前層數：". $event->getLine(2) ."樓");
			if(is_numeric($event->getLine(1)) === true){
				$event->setLine(1, TextFormat::YELLOW ."移動格數：". $event->getLine(1) ."格");
				$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::GREEN ."成功創建升降機");
			}
		}
		
		}
		
		}
		
	}
	
	
	public function onTapSign(PlayerInteractEvent $event){
		
		if ( ($event->getBlock()->getID() == 63 ) || ($event->getBlock()->getID() == 68) || ($event->getBlock()->getID() == 323)){
			
			$sign = $event->getPlayer()->getLevel()->getTile($event->getBlock());
			$p = $event->getPlayer();
		
		if ( $sign == null ) return ;
		if($sign->getText()[0] == "del" && $sign->getText()[1] == "del" && $sign->getText()[2] == "del" && $sign->getText()[3] == "del" ){
			
			$up = new Vector3($sign->x,$sign->y +1,$sign->z);
			$up = $event->getBlock()->getLevel()->getBlock($up);
			$down = new Vector3($sign->x,$sign->y -1,$sign->z);
			$down = $event->getBlock()->getLevel()->getBlock($down);
			
			if( $up->getId() === 63 || $up->getId() === 68 || $up->getId() === 323 ){
				
				$up = $event->getBlock()->getLevel()->getTile($up);
				
				if($up->getText()[0] == TextFormat::YELLOW ."[升降機]" || $up->getText()[0] == TextFormat::YELLOW ."[PAY]" || $up->getText()[0] == TextFormat::YELLOW ."[老虎機]" ){
					
					$up->setText(TextFormat::GREEN . "刪除成功，請拆掉木牌");
					
				}
				
			}
			
			if( $down->getId() === 63 || $down->getId() === 68 || $down->getId() === 323 ){
				
				$down = $event->getBlock()->getLevel()->getTile($down);
				
				if($down->getText()[0] == TextFormat::YELLOW ."[升降機]" || $down->getText()[0] == TextFormat::YELLOW ."[PAY]" || $down->getText()[0] == TextFormat::YELLOW ."[老虎機]"){
					
					$down->setText(TextFormat::GREEN . "刪除成功，請拆掉木牌");
					
				}
				
			}
			
		}
		
		if($sign->getText()[0] == TextFormat::YELLOW ."[升降機]"){
					
			if ( preg_match_all('/移動格數：(-?\d+)格/', $sign->getText()[1], $mcs ) == 0 ) {
				return;
			}
			$high = $mcs[1][0];
			
			if(is_numeric($high) === true){
				
				if($sign->getText()[2] == TextFormat::RED ."層數錯誤！" ){
					if($high < 1000 && $high > -129 && $high !== "0"){
						
					}else{
						$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的移動格數");
					}
					
					$p->sendPopup(TextFormat::YELLOW."原封不動xD");
					$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的層數");
					$sign->setText(TextFormat::GREEN . "請拆掉木牌");
				}elseif($high < 1000 && $high > -129){
					
					if($high < 0){
						$p->sendPopup(TextFormat::YELLOW."已經下了一層");
					} elseif ($high > 0){
						$p->sendPopup(TextFormat::YELLOW."已經上了一層"); 
					} else {
						$p->sendPopup(TextFormat::YELLOW."原封不動xD");
						$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的移動格數");
						$sign->setText(TextFormat::GREEN . "請拆掉木牌");
						if($sign->getText()[2] == TextFormat::RED ."層數錯誤！" ){
							$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的層數");
						}
						return;
					}
					
			$x = $sign->x + 0.5;
			$y = $sign->y - 1 + $mcs[1][0];
			$z = $sign->z + 0.5;
			$l = $sign->getLevel();
			$p->teleport(new Position($x,$y,$z,$l));
			// TP
				}else{
					$p->sendPopup(TextFormat::YELLOW."原封不動xD");
					$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的移動格數");
					$sign->setText(TextFormat::GREEN . "請拆掉木牌");
				}
			
			}else{
				
				if($sign->getText()[1] ==  TextFormat::RED ."移動格數錯誤！"){
					$p->sendPopup(TextFormat::YELLOW."原封不動xD");
					$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的移動格數");
					$sign->setText(TextFormat::GREEN . "請拆掉木牌");
				}
				if($sign->getText()[2] == TextFormat::RED ."層數錯誤！" ){
					$p->sendPopup(TextFormat::YELLOW."原封不動xD");
					$p->sendMessage(TextFormat::YELLOW ."[升降機]". TextFormat::RED ."請填上正確的層數");
					$sign->setText(TextFormat::GREEN . "請拆掉木牌");
				}
			}
		}
	}
	
	}
	
	public function onDisable(){
		
	}
}